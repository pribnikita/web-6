let data = {
    "prod1": {
        basePrice: 100
    },
    "prod2": {
        basePrice: 200,
        options: {
            "opt1": 5,
            "opt2": 10,
            "opt3": 15
        }
    },
    "prod3": {
        basePrice: 300,
        properties: {
            "prop1": 7,
            "prop2": 3
        }
    }
};

function updateCost() {
    let type = document.getElementById("type").value;

    document.getElementById("options").style.display = (
        type === "prod2"
        ? "block"
        : "none"
    );
    document.getElementById("checkboxes").style.display = (
        type === "prod3"
        ? "block"
        : "none"
    );

    let priceData = data[type];
    let price = priceData.basePrice;

    if (type === "prod2") {
        let options = document.querySelectorAll("#options input");
        options.forEach(function (option) {
            if (option.checked) {
                price += priceData.options[option.value];
            }
        });
    } else if (type === "prod3") {
        let properties = document.querySelectorAll("#checkboxes input");
        properties.forEach(function (property) {
            if (property.checked) {
                price += priceData.properties[property.name];
            }
        });
    }

    let r = document.getElementById("result");
    let amount = document.getElementById("amount").value;
    if (!(/^[1-9][0-9]*$/).test(amount)) {
        r.innerHTML = "Wrong amount!";
        return;
    }
    amount = parseInt(amount);
    r.innerHTML = "Price: " + price * amount;
}

window.addEventListener("DOMContentLoaded", function () {
    document.getElementById("options").style.display = "none";
    document.getElementById("checkboxes").style.display = "none";

    let select = document.getElementById("type");
    select.addEventListener("change", updateCost);

    let options = document.querySelectorAll("#options input");
    options.forEach(function (option) {
        option.addEventListener("change", updateCost);
    });

    let checkboxes = document.querySelectorAll("#checkboxes input");
    checkboxes.forEach(function (checkbox) {
        checkbox.addEventListener("change", updateCost);
    });

    let amount = document.getElementById("amount");
    amount.addEventListener("change", updateCost);

    updateCost();
});
